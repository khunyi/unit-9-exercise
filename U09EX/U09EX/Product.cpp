#include "Product.h"
using namespace std;

Product::Product()
{
	Setup("UNSET", 0, 0);
}

Product::Product(string newName, int newQuantity, float newPrice)
{
	Setup(newName, newQuantity, newPrice);
}

Product::Product(const Product& copyProduct)
{
	Setup(copyProduct.GetName(), copyProduct.GetQuantityInStock(), copyProduct.GetPrice());
}


//void Product::Setup(string newName)
//{
//	Setup(newName, 0, 0);
//}

void Product::Setup(string newName /*= "unset"*/, int newQuantity /*= 0*/, float newPrice /*= 0*/)
{
	m_name = newName;
	m_quantityInStock = newQuantity;
	m_price = newPrice;
}


string Product::GetName() const
{
	return m_name;
}

int Product::GetQuantityInStock() const
{
	return m_quantityInStock;
}

float Product::GetPrice() const
{
	return m_price;
}