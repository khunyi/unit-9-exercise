#ifndef _PRODUCT
#define _PRODUCT

#include <iostream>
#include <string>
using namespace std;


class Product
{
public:
	Product();
	Product(string newName, int newQuantity, float newPrice);
	Product(const Product& copyProduct);


	void Setup(string newName = "unset", int newQuantity = 0, float newPrice = 0);
	/*void Setup(string newName);*/


	string GetName() const;
	int GetQuantityInStock() const;
	float GetPrice() const;

private:
	string m_name;
	int m_quantityInStock;
	float m_price;


};



#endif